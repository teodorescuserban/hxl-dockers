listen      80;
access_log  /var/log/nginx/hxl_proxy.access.log;
error_log   /var/log/nginx/hxl_proxy.error.log;

return 301 https://%server_name%request_uri;

# or comment the line above and uncomment line below if you cant use http-to-https redirection
#include /etc/nginx/includes/hxl_proxy_include.conf;
