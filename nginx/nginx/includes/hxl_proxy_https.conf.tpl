listen      443 ssl;
charset     utf-8;
access_log  /var/log/nginx/hxl_proxy.access.log;
error_log   /var/log/nginx/hxl_proxy.error.log;

# max upload size
client_max_body_size 75M;

ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_ciphers   'AES256+EECDH:AES256+EDH';

include /etc/nginx/includes/hxl_proxy_include.conf;
