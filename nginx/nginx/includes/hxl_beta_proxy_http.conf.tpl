listen      80;
access_log  /var/log/nginx/hxl_beta_proxy.access.log;
error_log   /var/log/nginx/hxl_beta_proxy.error.log;

add_header Access-Control-Allow-Origin "*";
# return 301 https://beta-proxy.hxlstandard.org%request_uri;
return 301 https://%server_name%request_uri;

# or comment the line above and uncomment line below if you cant use http-to-https redirection
#include /etc/nginx/includes/hxl_beta_proxy_include.conf;
