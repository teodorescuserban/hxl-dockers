error_page 418 = /offline.html;

location / {
    #if (%maintenance = "yes") {
    #    return 418;
    #}
    proxy_redirect off;
    # proxy_pass http://93.114.48.221:80;
    proxy_pass http://172.17.42.1:81;
    include /etc/nginx/proxy_params;
}
