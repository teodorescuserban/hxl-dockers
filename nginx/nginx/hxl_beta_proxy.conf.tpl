server {
    include /etc/nginx/includes/hxl_beta_proxy_http.conf;
    server_name beta.proxy.hxlstandard.org beta-proxy.hxlstandard.org;
}

server {
    include /etc/nginx/includes/hxl_beta_proxy_https.conf;
    server_name beta.proxy.hxlstandard.org beta-proxy.hxlstandard.org;
    ssl_certificate /etc/nginx/ssl.crt;
    ssl_certificate_key /etc/nginx/ssl.key;
}
